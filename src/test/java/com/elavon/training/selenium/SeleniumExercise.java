package com.elavon.training.selenium;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;


public class SeleniumExercise {

	WebDriver driver;

    public WebDriver setupWebDriver(String browser) {
        if (browser == "firefox") {
            FirefoxDriverManager.getInstance().architecture(Architecture.x32).version(".18").setup();
            System.out.println("Debug 2");
            System.out.println("Debug 3");
            driver = new FirefoxDriver();
            System.out.println("Debug 4");
        }
        else if (browser == "chrome") {
            ChromeDriverManager.getInstance().setup();
            driver = new ChromeDriver();
        }
        else if (browser == "iexplorer") {
            InternetExplorerDriverManager.getInstance().setup();
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            driver = new InternetExplorerDriver();
        }
        else {
            InternetExplorerDriverManager.getInstance().setup();
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            driver = new InternetExplorerDriver();
        }
        return driver;
    }

    @Before
    public void setupTest() {
        driver = setupWebDriver("chrome");
    }


    @Test
    public void TestCase1() {
        String baseUrl = "http://demoqa.com/";
        driver.get(baseUrl);
        
        //Click registration
        driver.findElement(By.cssSelector("a[href='http://demoqa.com/registration/']")).click();
        
        //Fill fields
        WebElement firstName = driver.findElement(By.id("name_3_firstname"));
        firstName.sendKeys("Shainna");
        
        WebElement lastName = driver.findElement(By.id("name_3_lastname"));
        lastName.sendKeys("Shinozuka");
        
        WebElement mStatus = driver.findElement(By.cssSelector("[name='radio_4[]'][value='married']"));
        mStatus.click();
        
        WebElement myHobby1 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
        myHobby1.click();
        
        WebElement myHobby2 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='dance']"));
        myHobby2.click();
        
        Select country = new Select(driver.findElement(By.id("dropdown_7")));
        country.selectByVisibleText("United Kingdom");
        
        Select monthB = new Select(driver.findElement(By.id("mm_date_8")));
        monthB.selectByVisibleText("8");
        Select dayB = new Select(driver.findElement(By.id("dd_date_8")));
        dayB.selectByVisibleText("14");
        Select yearB = new Select(driver.findElement(By.id("yy_date_8")));
        yearB.selectByVisibleText("1997");
        
        WebElement phone = driver.findElement(By.id("phone_9"));
        phone.sendKeys("091512345678");
        
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("Star14");
        
        WebElement email = driver.findElement(By.id("email_1"));
        email.sendKeys("shainnamae_19@yahoo.com");
        
        driver.findElement(By.id("profile_pic_10")).sendKeys("C:/Users/Albert Merto Fermo/Downloads/4908262-resident-evil-wallpaper.jpg");
        
        WebElement description = driver.findElement(By.id("description"));
        description.sendKeys("Daydreamer.");
        
        WebElement password = driver.findElement(By.id("password_2"));
        password.sendKeys("heyd@ydreamer14");
        
        WebElement confirm = driver.findElement(By.id("confirm_password_password_2"));
        confirm.sendKeys("heyd@ydreamer14");
        
        WebElement submit = driver.findElement(By.name("pie_submit"));
        submit.click();
        
        driver.close();
    }
    @Test
    public void TestCase2() {
        String baseUrl = "http://demoqa.com/";
        driver.get(baseUrl);
        try {
            TimeUnit.SECONDS.sleep(15);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        //Click registration
        driver.findElement(By.cssSelector("a[href='http://demoqa.com/registration/']")).click();
        
        WebElement contactLink = driver.findElement(By.cssSelector("[href='http://demoqa.com/contact/']"));
        
        WebElement firstName = driver.findElement(By.id("name_3_firstname"));
        firstName.sendKeys("Renato");
        
        WebElement lastName = driver.findElement(By.id("name_3_lastname"));
        lastName.sendKeys("Abella");
        
        WebElement mStatus = driver.findElement(By.cssSelector("[name='radio_4[]'][value='single']"));
        mStatus.click();
        
        WebElement myHobby1 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
        myHobby1.click();
        WebElement myHobby2 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='cricket ']"));
        myHobby2.click();
        
        Select country = new Select(driver.findElement(By.id("dropdown_7")));
        country.selectByVisibleText("United Kingdom");
        
        Select monthB = new Select(driver.findElement(By.id("mm_date_8")));
        monthB.selectByVisibleText("2");
        Select dayB = new Select(driver.findElement(By.id("dd_date_8")));
        dayB.selectByVisibleText("13");
        Select yearB = new Select(driver.findElement(By.id("yy_date_8")));
        yearB.selectByVisibleText("1998");
        
        WebElement phone = driver.findElement(By.id("phone_9"));
        phone.sendKeys("09124567895");
        
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("Star14");
        
        WebElement email = driver.findElement(By.id("email_1"));
        email.sendKeys("renato06@gmail.com");
        
        driver.findElement(By.id("profile_pic_10")).sendKeys("C:/Users/Albert Merto Fermo/Downloads/4908262-resident-evil-wallpaper.jpg");
        
        WebElement desc = driver.findElement(By.id("description"));
        desc.sendKeys("Resident Evil");
        
        WebElement password = driver.findElement(By.id("password_2"));
        password.sendKeys("R3n@t005");
        
        WebElement confirm = driver.findElement(By.id("confirm_password_password_2"));
        confirm.sendKeys("R3n@t005");
        
        WebElement submit = driver.findElement(By.name("pie_submit"));
        submit.click();
        
        driver.close();
    }
}


